import bisect
import csv
import json

movies_dict = {}

with open("IMDb_movies.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_title_id')
    user_index = columns.index('reviews_from_users')
    critic_index = columns.index('reviews_from_critics')

    for movie in parsed:
        movies_dict[movie[movie_id]] = {"critic_reviews": movie[critic_index], "user_reviews": movie[user_index]}

with open("IMDb_ratings.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_title_id')
    total_votes_id = columns.index('total_votes')
    top_voters_id = columns.index('top1000_voters_rating')
    top_voters_num_id = columns.index('top1000_voters_votes')
    weighted_average_id = columns.index('weighted_average_vote')
    for movie in parsed:
        if movie[movie_id] not in movies_dict:
            movies_dict[movie[movie_id]] = {"top_voters_rating": movie[top_voters_id], "total_votes": movie[total_votes_id], "top_voters_number": movie[top_voters_num_id], "average": movie[weighted_average_id]}
        else:
            movies_dict[movie[movie_id]]["top_voters_rating"] = movie[top_voters_id]
            movies_dict[movie[movie_id]]["total_votes"] = movie[total_votes_id]
            movies_dict[movie[movie_id]]["top_voters_number"] = movie[top_voters_num_id]
            movies_dict[movie[movie_id]]["average"] = movie[weighted_average_id]

with open("processed_ratings.csv", "w+") as write_file:
    write_file.write("movie_id,average,total_votes,reviews_from_users,reviews_from_critics,top_voters_rating,top_voters_number\n")
    for key in movies_dict:
        movie = movies_dict[key] 
        average = movie["average"] if "average" in movie else ""
        critic_reviews = movie["critic_reviews"] if "critic_reviews" in movie else ""
        user_reviews = movie["user_reviews"] if "user_reviews" in movie else ""
        total_votes = movie["total_votes"] if "total_votes" in movie else ""
        top_voters_rating = movie["top_voters_rating"] if "top_voters_rating" in movie else ""
        top_voters_number = movie["top_voters_number"] if "top_voters_number" in movie else ""
        
        write_file.write("{},{},{},{},{},{},{}\n".format(key, average, total_votes, user_reviews, critic_reviews, top_voters_rating, top_voters_number))



with open("IMDb_names.csv", "r") as csv_file:
    with open("formatted_name_data.csv", "w+") as out_file:
        for line in csv_file:
            if line[:2] == "nm" or line[:12] == "imdb_name_id":
                out_file.write(line)

import bisect
import csv
import json
import statistics

movie_mappings = {}
movie_ratings = {}
movies_dict = {}
genres_set = set()

with open("IMDb_movies.csv", "r", encoding='utf8') as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_title_id')

    for movie in parsed:
        movies_dict[movie[movie_id][2:]] = []

with open("links.csv", "r", encoding="utf8") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    imdb_id = columns.index('imdbId')
    movie_id = columns.index('movieId')
    for movie in parsed:
        if movie[imdb_id] not in movies_dict:
            continue
        movie_mappings[movie[movie_id]] = movie[imdb_id]

with open("ratings.csv", "r", encoding="utf8") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('movieId')
    rating_index = columns.index('rating')
    for movie in parsed:
        if movie[movie_id] not in movie_mappings:
            continue
        movies_dict[movie_mappings[movie[movie_id]]].append(float(movie[rating_index]))

with open("movie_alt_ratings.csv", "w+") as write_file:
    write_file.write("movie_id,average_rating,num_ratings\n")
    for movie in movies_dict:
        if len(movies_dict[movie]) < 1:
            continue
        write_file.write("tt{},{},{}\n".format(movie, statistics.mean(movies_dict[movie]), len(movies_dict[movie])))


import csv

roles_dict = {}

with open("IMDb_title_principals.csv", "r") as f:
    parsed = csv.reader(f, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)
    
    movie_id = columns.index('imdb_title_id')
    person_id = columns.index('imdb_name_id')
    character_id = columns.index('characters')
    role_index = columns.index('category')
    for entry in parsed:
        if entry[movie_id] not in roles_dict:
            roles_dict[entry[movie_id]] = {"director": [], "actors": {}, "editor": [], "archive_sound": [], "producer": [], "archive_footage": [], "writer": [], "composer": [], "production_designer": [], "cinematographer": []}
        if entry[role_index] == "self" or entry[role_index] == "actor" or entry[role_index] == "actress":
            roles_dict[entry[movie_id]]["actors"][entry[person_id]] = entry[character_id].replace("\"", "").replace("[", "\"").replace("]", "\"")
        else:
            roles_dict[entry[movie_id]][entry[role_index]].append(entry[person_id])

with open("processed_roles.csv", "w+") as write_file:
    write_file.write("movie_id,person_id,role\n")
    for key in roles_dict:
        entry = roles_dict[key]
        if entry["director"] != []:
            for director in entry["director"]:
                write_file.write("{},{},{}\n".format(key, director,"director"))
        if entry["production_designer"] != []:
            for pd in entry["production_designer"]:
                write_file.write("{},{},{}\n".format(key, pd,"production_designer"))
        if entry["cinematographer"] != []:
            for cg in entry["cinematographer"]:
                write_file.write("{},{},{}\n".format(key, cg,"cinematographer"))
        if entry["editor"] != []:
            for editor in entry["editor"]:
                write_file.write("{},{},{}\n".format(key, editor,"editor"))
        if entry["archive_sound"] != []:
            for archive_sound in entry["archive_sound"]:
                write_file.write("{},{},{}\n".format(key, archive_sound,"archive_sound"))
        if entry["archive_footage"] != []:
            for archive_footage in entry["archive_footage"]:
                write_file.write("{},{},{}\n".format(key, archive_footage,"archive_footage"))
        if entry["producer"] != []:
            for producer in entry["producer"]:
                write_file.write("{},{},{}\n".format(key, producer,"producer"))
        if entry["writer"] != []:
            for writer in entry["writer"]:
                write_file.write("{},{},{}\n".format(key, writer,"writer"))
        if entry["composer"] != []:
            for composer in entry["composer"]:
                write_file.write("{},{},{}\n".format(key, composer,"composer"))

with open("processed_actors.csv", "w+") as write_file:
    write_file.write("movie_id,person_id,role,characters\n")
    for key in roles_dict:
        entry = roles_dict[key]
        if entry["actors"] != {}:
            for actor_key in entry["actors"]:
                role = entry["actors"][actor_key]
                if role == "":
                    role = '""'
                write_file.write("{},{},{},{}\n".format(key, actor_key, "actor/actress", role))


import bisect
import csv
import json

movies_dict = {}
movie_mappings = {}
keywords_set = set()

with open("IMDb_movies.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_title_id')

    for movie in parsed:
        movies_dict[movie[movie_id]] = []

with open("links.csv", "r", encoding="utf8") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    imdb_id = columns.index('imdbId')
    movie_id = columns.index('tmdbId')
    for movie in parsed:
        if "tt"+movie[imdb_id] not in movies_dict:
            continue
        movie_mappings[movie[movie_id]] = movie[imdb_id]

with open("keywords.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('id')
    keyword_index = columns.index('keywords')
    for movie in parsed:
        if movie[movie_id] not in movie_mappings:
            continue
        imdb_id = movie_mappings[movie[movie_id]]
        try:
            formatted = movie[keyword_index].replace("\"\"", "\\\"").replace("'id'", "\"id\"").replace("'name'", "\"name\"").replace(": '", ": \"").replace("'}", "\"}").replace("\\xa0", "")
            parsed_json = json.loads(formatted)
        except json.decoder.JSONDecodeError:
            formatted = movie[keyword_index].replace("\"", "\\\"").replace("'id'", "\"id\"").replace("'name'", "\"name\"").replace(": '", ": \"").replace("'}", "\"}").replace("\\xa0", "")
            parsed_json = json.loads(formatted)
        for keyword in parsed_json:
            movies_dict["tt"+imdb_id].append(keyword["name"])
            keywords_set.add(keyword["name"])
       
with open("processed_keywords.csv", "w+") as write_file:
    write_file.write("movie_id,keyword\n")
    for movie in movies_dict:
        for keyword in movies_dict[movie]:
            if keyword != "":
                write_file.write("{},\"{}\"\n".format(movie, keyword))

with open("keywords_list.csv", "w+") as write_file:
    write_file.write("keyword\n")
    for keyword in keywords_set:
        if keyword != "":
            write_file.write("\"{}\"\n".format(keyword))


import bisect
import csv
import json

movie_languages = {}
movie_countries = {}
movie_companies = {}
production_company_set = set()
country_set = set()
language_set = set()

with open("IMDb_movies.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_title_id')
    pc_index = columns.index('production_company')
    c_index = columns.index('country')
    l_index = columns.index('language')

    for movie in parsed:
        pc = sorted([x.strip() for x in movie[int(pc_index)].split(",")])
        l = sorted([x.strip() for x in movie[int(l_index)].split(",")])
        c = sorted([x.strip() for x in movie[int(c_index)].split(",")])
        for company in pc:
            production_company_set.add(company)
        movie_companies[movie[movie_id]] = pc
        for language in l:
            language_set.add(language)
        movie_languages[movie[movie_id]] = l
        for country in c:
            country_set.add(country)
        movie_countries[movie[movie_id]] = c

with open("movies_metadata.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_id')
    pc_index = columns.index('production_companies')
    c_index = columns.index('production_countries')

    for movie in parsed:
        if movie[movie_id] not in movie_languages:
            continue

        debug = movie
        try:
            formatted = movie[pc_index].replace("\"", "\\\"").replace("'id'", "\"id\"").replace("'name'", "\"name\"").replace(": '", ": \"").replace("\\\",","\\\"\",").replace("',", "\",").replace(": \\\"", ": \"\\\"").replace("\\xa0", "")
            formatted = json.loads(formatted)
        except IndexError:
            print(debug)
            continue
        if len(formatted) > len(movie_companies[movie[movie_id]]):
            movie_companies[movie[movie_id]] = sorted([x["name"] for x in formatted])
        formatted = movie[c_index].replace("'iso_3166_1'", "\"iso_3166_1\"").replace("'name'", "\"name\"").replace(": '", ": \"").replace("',", "\",").replace("\'}", "\"}")
        formatted = json.loads(formatted)
        if len(formatted) > len(movie_countries[movie[movie_id]]):
            movie_countries[movie[movie_id]] = sorted([x["name"] for x in formatted])

with open("countries.csv", "w+") as write_file:
    write_file.write("country\n")
    for country in sorted(list(country_set)):
        if country == "":
            continue
        write_file.write("{}\n".format(country))

with open("languages.csv", "w+") as write_file:
    write_file.write("languages\n")
    for language in sorted(list(language_set)):
        if language == "":
            continue
        write_file.write("{}\n".format(language))

with open("production_companies.csv", "w+") as write_file:
    write_file.write("production_company\n")
    for company in sorted(list(production_company_set)):
        if company == "":
            continue
        write_file.write("{}\n".format(company))

with open("movie_countries.csv", "w+") as write_file:
    write_file.write("movie_id,country\n")
    for movie in movie_countries:
        for country in movie_countries[movie]:
            write_file.write("{},{}\n".format(movie, country))

with open("movie_languages.csv", "w+") as write_file:
    write_file.write("movie_id,language\n")
    for movie in movie_languages:
        for language in movie_languages[movie]:
            write_file.write("{},{}\n".format(movie, language))

with open("movie_production_companies.csv", "w+") as write_file:
    write_file.write("movie_id,company\n")
    for movie in movie_companies:
        for company in movie_companies[movie]:
            write_file.write("{},{}\n".format(movie, company))


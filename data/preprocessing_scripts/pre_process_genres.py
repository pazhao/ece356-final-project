import bisect
import csv
import json

movies_dict = {}
genres_set = set()

with open("IMDb_movies.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_title_id')
    genre_index = columns.index('genre')

    for movie in parsed:
        genres = sorted([x.strip() for x in movie[int(genre_index)].split(",")])
        for genre in genres:
            genres_set.add(genre)
        movies_dict[movie[movie_id]] = genres

with open("movies_metadata.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('imdb_id')
    genre_index = columns.index('genres')
    for movie in parsed:
        if movie[movie_id] not in movies_dict:
            continue
        for genre in json.loads(movie[genre_index].replace("'", "\"")):
            if genre["name"].lower() not in (name.lower() for name in movies_dict[movie[movie_id]]):
                bisect.insort(movies_dict[movie[movie_id]], genre["name"])

with open("Mojo_budget_update.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('movie_id')
    genre1_index = columns.index('genre_1')
    genre4_index = columns.index('genre_4')
    for movie in parsed:
        if movie[movie_id] not in movies_dict:
            continue
        for genre in movie[genre1_index:genre4_index+1]:
            if genre == "":
                break
            if genre.lower() not in (name.lower() for name in movies_dict[movie[movie_id]]):
                bisect.insort(movies_dict[movie[movie_id]], genre)

with open("genres.csv", "w+") as write_file:
    write_file.write("genre\n")
    for genre in genres_set:
        write_file.write("{}\n".format(genre))

with open("movie_genres.csv", "w+") as write_file:
    write_file.write("movie_id,genre\n")
    for movie in movies_dict:
        for genre in movies_dict[movie]:
            write_file.write("{},{}\n".format(movie, genre))


import csv

with open("Mojo_budget_update.csv", "r") as csv_file:
    parsed = csv.reader(csv_file, quotechar="\"")

    parsed_iter = iter(parsed)
    columns = next(parsed_iter)

    movie_id = columns.index('movie_id')
    budget = columns.index('budget')
    domestic = columns.index('domestic')
    international = columns.index('international')
    worldwide = columns.index('worldwide')
   
    with open("processed_money.csv", "w+") as write_file:
        write_file.write("movie_id,budget,domestic,international,worldwide\n")
        for line in parsed:
            i_budget = line[budget]
            i_domestic = line[domestic]
            i_international = line[international]
            i_worldwide = line[worldwide]

            if i_domestic == "" and i_international != "" and i_worldwide != "":
                i_domestic = float(i_worldwide) - float(i_international)
            elif i_domestic != "" and i_international == "" and i_worldwide != "":
                i_international = float(i_worldwide) - float(i_domestic)
            elif i_domestic != "" and i_international != "" and i_worldwide == "":
                i_worldwide = float(i_domestic) - float(i_international)
            elif i_domestic != "" and i_international != "" and i_worldwide != "":
                pass
            else:
                if i_domestic == "":
                    i_domestic = -1
                if i_international == "":
                    i_international = -1
                if i_worldwide == "":
                    i_worldwide = -1


            write_file.write("{},{},{},{},{}\n".format(line[0], i_budget, i_domestic, i_international, i_worldwide))



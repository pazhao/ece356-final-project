import mysql.connector 
import datetime

class DB:
    # For connecting to DB
    host = "127.0.0.1"
    port = "12345"
    user = "root"
    password = ""
    database = "movies"
    connection = None
    cursor = None

    # For obtaining a mysql connector object
    @staticmethod
    def get_connector():
        if DB.connection is None:
            DB.connection = mysql.connector.connect(
                    host=DB.host,
                    port=DB.port,
                    user=DB.user,
                    password=DB.password,
                    database=DB.database
                    )
        return DB.connection
    
    # For obtaining a mysql cursor object
    @staticmethod
    def get_cursor():
        if DB.connection is None:
            connector()
        if DB.cursor is None:
            DB.cursor = DB.connection.cursor()
        return DB.cursor

# This class is used to parse arguments and formatting for a query
class QueryParsing:

    # This function takes in a set of column_names that are valid and verifies that the input columns are indeed in the column_names.
    # It then returns the columns that are valid whilst outputting the columns that have been left behind as invalid
    @staticmethod
    def parse_columns(column_names, input_columns):
        # If we want all columns, and the input columns is just "*", then return the full set of possible columns
        if input_columns == ["*"]:
            return column_names
        problem_columns = []
        valid_columns = []
        for c in input_columns:
            if c not in column_names:
                problem_columns.append(c)
            else:
                valid_columns.append(c)
        if len(problem_columns) != 0:
            print("Invalid columns:")
            print('\n'.join(problem_columns))
            print("Continuing.")
        return valid_columns
   
    # This function takes in the dictionary that represents mapping from a human-name to a table name, and checks that the human-name is valid
    @staticmethod
    def parse_joins(mappings, input_tables):
        problem_tables = []
        valid_tables = []
        for t in input_tables:
            if t not in mappings:
                problem_tables.append(t)
            else:
                valid_tables.append(mappings[t])
        if len(problem_tables) != 0:
            print("Invalid columns:")
            print('\n'.join(problem_tables))
            print("Continuing.")
        return valid_tables
    
    # This function takes in the a list of column names, followed by a sample line from a table, followed by the column that we are trying
    #   to define a criteria around.
    # For example, we want to define a criteria around "title", and we have a sample line that has a title in it, with all the column names.
    @staticmethod
    def parse_criteria(column_names, table_types, column):
        print(f"Current column: {column}")
        print("Possible criteria:")
        # Find the type of `column` so we know what operations are permitted on it. Specifically, with the sample tuple, find the corresponding index that corresponds to the 
        # column we want to change, and then find its type. 
        column_type = type(table_types[column_names.index(column)])

        # Once we know the type, we can if branch on it, and define the appropriate operators that are allowed
        # Since the logic behind each type is similar, I will simply comment the most complicated type, which is datetime.
        # datetime should be on the third branch, or the second elif branch, on line 143.
        if column_type == int:
            print("<, >, =, <>")
            operator = None
            for _ in range(3):
                operator = input("Select a criteria:\n")
                if operator not in ["<", ">", "=", "<>"]:
                    print("Invalid criteria.")
                    operator = None
                else:
                    break
            if operator is None:
                print("Failed. Skipping column.")
                return
            argument = None
            for _ in range(3):
                try:
                    argument = int(input("Enter an argument:\n"))
                    break
                except ValueError:
                    print("Invalid argument")
                    argument = None
            if operator is None:
                print("Failed. Skipping column.")
                return
            return f"{column} {operator} {argument}"
        elif column_type == float:
            print("<, >, =, <>")
            operator = None
            for _ in range(3):
                operator = input("Select a criteria:\n")
                if operator not in ["<", ">", "=", "<>"]:
                    print("Invalid criteria.")
                    operator = None
                else:
                    if operator == "=":
                        operator = "LIKE"
                    elif operator == "<>":
                        operator = "NOT LIKE"
                    break
            if operator is None:
                print("Failed. Skipping column.")
                return
            argument = None
            for _ in range(3):
                try:
                    argument = float(input("Enter an argument:\n"))
                    break
                except ValueError:
                    print("Invalid argument")
                    arugment = None
            if operator is None:
                print("Failed. Skipping column.")
                return
            return f"{column} {operator} {argument}"
        elif column_type == datetime.date:
            # We will support <. >, =, and <> comparisons with datetime.
            print("<, >, =, <>")
            # First, set our operator to none.
            operator = None
            # We will allow the user to try to select an operator 3 times.
            for _ in range(3):
                operator = input("Select a criteria:\n")
                if operator not in ["<", ">", "=", "<>"]:
                    print("Invalid criteria.")
                    operator = None
                else:
                    # we have a valid operator, break
                    break
            # We still don't have a valid operator after three times, we failed, just skip this column
            if operator is None:
                print("Failed. Skipping column.")
                return

            # Now, the user enters a value that they want to compare to. For example, say the attribute in the tuple
            # is called birthday. We already selected our operator above. Let's say our operator is =.
            # A comparison might look like [...] WHERE birthday = date [...];
            # we are choosing the "date" part of the above query.
            argument = None
            for _ in range(3):
                try:
                    argument = input("Enter an argument - dates should be formatted like 'YYYY-MM-DD':\n")
                    argument = "'{}'".format(datetime.datetime.strptime(argument.strip("'"), '%Y-%m-%d').date())
                    break
                except ValueError:
                    print("Invalid argument")
                    argument = None
            if operator is None:
                print("Failed. Skipping column.")
                return
            return f"{column} {operator} {argument}"
        elif column_type == str:
            print("in, not in")
            operator = None
            for _ in range(3):
                operator = input("Select a criteria:\n")
                if operator not in ["in", "not in"]:
                    print("Invalid criteria.")
                    operator = None
                else:
                    break
            if operator is None:
                print("Failed. Skipping column.")
                return
            argument = None
            for _ in range(3):
                try:
                    argument = str(input("Enter an argument:\n"))
                    break
                except ValueError:
                    print("Invalid argument")
                    argument = None
            if operator is None:
                print("Failed. Skipping column.")
                return
            if operator == "in":
                return f"{column} LIKE \"%{argument}%\""
            else:
                return f"{column} NOT LIKE \"%{argument}%\""

    # This function takes in a column that we want to sort the data by. column_names has been included in this function,
    # in case input validation will be implemented later.
    @staticmethod
    def parse_order(column_names, column):
        print(f"Current column: {column}")
        order = None
        for _ in range(3):
            order = input("Type 'A' to sort by ascending order, or 'D' to sort by descending order.\n")
            # Try to be loose with what the user can type in
            if order not in ["A", "'A'", "D", "'D'"]:
                print("Invalid criteria.")
                order = None
            else:
                break
        if order is None:
            # If we tried three time and the user did not enter a valid string, we skip this column.
            print("Failed. Skipping column.")
            return
        # Mapping possible user inputs to the appropriate SQL string.
        order_mapping = {"A": "ASC", "'A'": "ASC", "D": "DESC", "'D'": "DESC"}
        print(f"{column} {order_mapping[order]}")
        return f"{column} {order_mapping[order]}"


# Represents an un-elevated user, i.e. the guest.
class User:
    def __init__(self): # Init - we need the connector and cursor
        self.authed = False
        self.id = 0
        self.connector = DB.get_connector()
        self.cursor = DB.get_cursor()

    def generate_movie_search(self):
        # Prep initial barebones query
        table_name = "Movies"
        self.cursor.execute("SELECT * FROM Movies LIMIT 1") # Hacky way to get column names
        column_names = [table_name+'.'+c.lower() for c in self.cursor.column_names] # Get a list of Table.attributes for users
        table_types = self.cursor.fetchone() # Get a sample line, so that we can call type() on each part of the table_types for QueryParsing.

        print("Possible properties of movies to view:")
        print(', '.join(column_names))

        # Get rows we want
        input_columns = input("Input desired columns, in order, separated by spaces, or ONLY * for all. Type \"more\" to search by advanced criterion:\n").split()
        if input_columns == ["more"]: # Does user want to search on attributes that are "supplementary" or advanced and in other tables?
            # Yes, they do.
            print("Advanced criterion:")
            valid_joins = input("Genres, Keywords, Finances, Languages, Countries, IMDBRatings, TMDBRatings, MoviesCrew, Features\n").split()
            # Get a mapping of human friendly names to our actual table names.
            other_tables = {"Genres": "MoviesGenres", "Keywords": "MoviesKeywords", "Finances": "Finances", "Countries": "MoviesCountries", "IMDBRatings": "IMDBRatings", "TMDBRatings": "TMDBRatings", "Languages": "MoviesLanguages", "Features": "Features", "MoviesCrew": "MoviesCrew"}
            # Make sure all the tables that the user inputs are valid.
            valid_joins = QueryParsing.parse_joins(other_tables, valid_joins)
           
            for t in valid_joins:
                # Add the new table to the inner join, so that we have expanded our table's attributes
                table_name += f" INNER JOIN {t} USING (movie_id) "
                self.cursor.execute(f"SELECT * FROM {t} LIMIT 1") # Hacky way to get column names of our new table
                # Add the new column names to our pre-existing column names
                column_names.extend([t+'.'+c.lower() for c in self.cursor.column_names if c != 'movie_id'])
                # Clear the cursor
                self.cursor.fetchall()
                # Get the column names of our new full-width table
                self.cursor.execute(f"SELECT * FROM {table_name} LIMIT 1") # Hacky way to get column names
                # Get a tuple for type processing
                table_types = self.cursor.fetchone()

            print(', '.join(column_names))
            # Get the user to select some columns that they want to see
            input_columns = input("Input desired columns, in order, separated by spaces, or ONLY * for all:\n").split()

        # Check the user's columns they want to see are valid.
        display_columns = QueryParsing.parse_columns(column_names, input_columns)
        
        # Get rows to search on
        input_columns = input("Input columns to search on:\n").split()
        search_columns = QueryParsing.parse_columns(column_names, input_columns)
        search_criterion = []
        for s in search_columns:
            # For each row that the user wants to search on, check that it's valid
            pc_result = QueryParsing.parse_criteria(column_names, table_types, s)
            if pc_result != None:
                search_criterion.append(pc_result)

        # Get rows to order by
        input_columns = input("Input columns to sort by, in order, separated by spaces:\n").split()
        sort_columns = QueryParsing.parse_columns(column_names, input_columns)
        sort_order = []
        for s in sort_columns:
            # For each row that the user wants to order on, check that it's valid
            po_result = QueryParsing.parse_order(column_names, s)
            if po_result != None:
                sort_order.append(po_result)

        # Construct query 
        query_string = f"SELECT {','.join(display_columns)} FROM {table_name}" # Base query
        if len(search_criterion) > 0:
            query_string += f" WHERE {' AND '.join(search_criterion)}" # If we have search criteria, add these to the where clause
        if len(sort_columns) > 0:
            query_string += f" ORDER BY {','.join(sort_order)}" # If we have sorting/ordering criteria, add these to the sort clause, in order.

        # Execute, and show 25 at a time.
        self.cursor.execute(query_string)
        tuples = self.cursor.fetchmany(25)
        print(self.cursor.column_names)
        for row in tuples:
            print(row)

        # While we're not completely out of results
        while len(tuples) == 25:
            more = input("Display more? y/n\n") # Check if the user wants to see more
            if more == "y":
                tuples = self.cursor.fetchmany(25)
                for row in tuples:
                    print(row)
            elif more == "n": # If we're done, clear the cursor, and then break
                self.cursor.fetchall()
                break
            else:
                print("Didn't understand. Try again.") # User entered something other than y/n

    # FUNCTIONALLY, THIS IS *IDENTICAL* TO generate_movie_search EXCEPT FOR TABLE NAMES. PLEASE SEE ABOVE FOR CODE EXPLANATION.
    def generate_people_search(self):
        # Prep initial
        table_name = "Person"
        self.cursor.execute("SELECT * FROM Person LIMIT 1") # Hacky way to get column names
        column_names = [table_name+'.'+c.lower() for c in self.cursor.column_names]
        table_types = self.cursor.fetchone()

        print("Possible properties of Persons to view:")
        print(', '.join(column_names))

        # Get rows we want
        input_columns = input("Input desired columns, in order, separated by spaces, or ONLY * for all. Type \"more\" to search by advanced criterion:\n").split()
        if input_columns == ["more"]:
            print("Advanced criterion:")
            valid_joins = input("DeathInfo\n").split()
            other_tables = {"DeathInfo": "DeathInfo"}
            valid_joins = QueryParsing.parse_joins(other_tables, valid_joins)
            
            for t in valid_joins:
                table_name += f" INNER JOIN {t} USING (person_id) "
                self.cursor.execute(f"SELECT * FROM {t} LIMIT 1") # Hacky way to get column names
                column_names.extend([t+'.'+c.lower() for c in self.cursor.column_names if c != 'person_id'])
                self.cursor.fetchall()
                self.cursor.execute(f"SELECT * FROM {table_name} LIMIT 1") # Hacky way to get column names
                table_types = self.cursor.fetchone()

            print(', '.join(column_names))
            input_columns = input("Input desired columns, in order, separated by spaces, or ONLY * for all:\n").split()

        display_columns = QueryParsing.parse_columns(column_names, input_columns)
        
        # Get rows to search on
        input_columns = input("Input columns to search on:\n").split()
        search_columns = QueryParsing.parse_columns(column_names, input_columns)
        search_criterion = []
        for s in search_columns:
            pc_result = QueryParsing.parse_criteria(column_names, table_types, s)
            if pc_result != None:
                search_criterion.append(pc_result)

        # Get rows to order by
        input_columns = input("Input columns to sort by, in order, separated by spaces:\n").split()
        sort_columns = QueryParsing.parse_columns(column_names, input_columns)
        sort_order = []
        for s in sort_columns:
            po_result = QueryParsing.parse_order(column_names, s)
            if po_result != None:
                sort_order.append(po_result)

        # Construct query 
        query_string = f"SELECT {','.join(display_columns)} FROM {table_name}"
        if len(search_criterion) > 0:
            query_string += f" WHERE {' AND '.join(search_criterion)}"
        if len(sort_columns) > 0:
            query_string += f" ORDER BY {','.join(sort_order)}"

        self.cursor.execute(query_string)
        tuples = self.cursor.fetchmany(size=25)
        print(self.cursor.column_names)
        for row in tuples:
            print(row)

        while tuples != []:
            more = input("Display more? y/n\n")
            if more == "y":
                tuples = self.cursor.fetchmany(size=25)
                for row in tuples:
                    print(row)
            elif more == "n":
                self.cursor.fetchall()
                break
            else:
                print("Didn't understand. Try again.")


# Code for an elevated user
class AuthUser(User):
    def __init__(self):
        super().__init__()
        self.authed = True # Just use the constructor of the superclass, but change the appropriate property
    
    def generate_add_review(self, arg=None):
        movie_id = None
        if arg != None:
            # This is in case we are called by generate_rewrite_review, where they want to edit a review that does not exist
            print("Review does not exists. Proceeding with new review.")
            movie_id = arg
        else:
            # We are creating a brand new review, we need a movie id.
            movie_id = input("Enter movie id. To get movie ids, exit using ctrl+C and use 'sm' to search for movie info.\n")
        
        # Let's make sure this movie exists, before we fail due to an FK restraint
        self.cursor.execute(f"SELECT * FROM Movies WHERE movie_id='{movie_id}'")
        movie = self.cursor.fetchone()
        if movie is None:
            # If the movie id doesn't exist, we'll come into here, and exit
            print("Couldn't find that movie. Exiting.")
            return
        else:
            # Prepare for new review
            self.cursor.execute(f"SELECT * FROM ClientInfo WHERE movie_id='{movie_id}'")
            if len(self.cursor.fetchall()) != 0:
                # If we already have a review with this movie_id, we want to rewrite this, not create a new one
                return self.generate_rewrite_review(arg=movie_id)
            # Prompt user for info
            rating = input("Add your rating (between 1 and 10):\n")
            review = input("Add your review:\n")
            favourite = input("Favourite? Enter 'TRUE' or 'FALSE'\n").strip("\'")

            # Execute and commit
            self.cursor.execute(f"INSERT INTO ClientInfo(movie_id, custom_rating, custom_review, favorited) VALUES ('{movie_id}',{rating},\"{review}\",{favourite});")
            self.connector.commit()
            return

    def generate_rewrite_review(self, arg=None):
        movie_id = None
        if arg != None:
            # This is in case we are called by generate_add_review, where they want to add a review that already exists
            print("Review already exists. Proceeding with review rewrite.")
            movie_id = arg
        else:
            # We are creating a brand new review, we need a movie id.
            movie_id = input("Enter movie id. To get movie ids, exit using ctrl+C and use 'sm' to search for movie info.\n")
        
        # Let's make sure this movie exists, before we fail due to an FK restraint
        self.cursor.execute(f"SELECT * FROM Movies WHERE movie_id='{movie_id}'")
        movie = self.cursor.fetchone()
        if movie is None:
            # If the movie id doesn't exist, we'll come into here, and exit
            print("Couldn't find that movie. Exiting.")
            return
        else:
            # Prepare to rewrite review.
            self.cursor.execute(f"SELECT * FROM ClientInfo WHERE movie_id='{movie_id}'")
            movie = self.cursor.fetchone()
            if movie is None:
                # If we already don't a review with this movie_id, we want to create a new one, not rewrite this
                self.generate_add_review(movie_id)
                return
            # Prompt user for info while informing them what they had before
            print("Your prior rating was:")
            print(movie[1])
            print("Your prior review was:")
            print(movie[2])
            if movie[3]:
                print("This is a favourited movie.")
            rating = input("Add your new rating (between 1 and 10):\n")
            review = input("Add your new review:\n")
            favourite = input("Favourite? Enter 'TRUE' or 'FALSE'\n").strip("\'")
            
            # Execute and commit
            self.cursor.execute(f"UPDATE ClientInfo SET custom_review = \"{review}\", custom_rating = {rating}, favorited = {favourite} WHERE movie_id = '{movie_id}';")
            self.connector.commit()
            return
    
    def generate_delete_review(self):
        # Let's make sure this movie exists, before we fail due to an FK restraint
        movie_id = input("Enter movie id. To get movie ids, exit using ctrl+C and use 'sm' to search for movie info.\n")
        self.cursor.execute(f"SELECT * FROM Movies WHERE movie_id='{movie_id}'")
        movie = self.cursor.fetchone()
        if movie is None:
            # Couldn't find the movie, exit - we can't even delete a review for a movie that doesn't exist
            print("Couldn't find that movie. Exiting.")
            return
        else:
            self.cursor.execute(f"SELECT * FROM ClientInfo WHERE movie_id='{movie_id}'")
            movie = self.cursor.fetchall()
            if len(movie) == 0:
                # The review is already non-existent, we are done
                return

            # Execute and commit
            self.cursor.execute(f"DELETE FROM ClientInfo WHERE movie_id = '{movie_id}';")
            self.connector.commit()
            return

    def generate_view_review(self):
        # Let's make sure this movie exists, before we fail due to an FK restraint
        movie_id = input("Enter movie id. To get movie ids, exit using ctrl+C and use 'sm' to search for movie info.\n")
        self.cursor.execute(f"SELECT * FROM Movies WHERE movie_id='{movie_id}'")
        movie = self.cursor.fetchone()
        if movie is None:
            print("Couldn't find that movie. Exiting.")
            return
        else:
            # Our movie exists, check if a review exists
            self.cursor.execute(f"SELECT * FROM ClientInfo WHERE movie_id='{movie_id}'")
            movie = self.cursor.fetchone()
            if movie is None:
                # Doesn't exist, exit
                print("This movie has not been rated, reviewed, nor favourited.")
                return
            # Print our column names and our review
            print(self.cursor.column_names)
            print(movie)
            return

    def generate_create_movie(self):
        # Prompt the user for the required info in t he Movies relation. We will deal with whether or not they want to add supplemental info below.
        movie_id = "\"" + input("Enter a movie_id that uniquely identifies the movie.\n") + "\""
        adult = input("Is this an adult film? Input 'TRUE' or 'FALSE'\n").strip("'")
        title = "\"" + input("Enter a title.\n") + "\""
        release_date = input("Enter a release date, in format 'YYYY-MM-DD'.\n")
        release_date = "'{}'".format(datetime.datetime.strptime(release_date.strip("'"), '%Y-%m-%d').date())
        duration = input("Enter a duration, in minutes.\n")
        description = "\"" + input("Enter a description.\n") + "\""
        mpaa = "\"" + input("Enter an mpaa rating.\n") + "\""
        self.cursor.execute(f"INSERT INTO Movies(movie_id, adult, title, release_date, duration, description, mpaa) VALUES({movie_id}, {adult}, {title}, {release_date}, {duration}, {description}, {mpaa});")

        # All our tables that represent a relationship with our movie.
        FK_Tables = ["ClientInfo", "Features", "Finances", "IMDBRatings", "TMDBRatings", "MoneyInfo", "MoviesCountries", "MoviesCrew", "MoviesGenres", "MoviesKeywords", "MoviesLanguages"]
       
        # For each of these tables:
        for i in FK_Tables:
            # check if the user wants to do anything with this table.
            if input(f"Add an {i} entry? y/n\n") == 'y':
                # If so, find the column_names, the column_types.
                self.cursor.execute(f"SELECT * FROM {i} LIMIT 1")
                types = self.cursor.fetchall()[0]
                columns = self.cursor.column_names

                # We are ready. Looping through column_names...
                args = []
                for idx, _ in enumerate(self.cursor.column_names):
                    # We already have the movie_id, so we can skip this one
                    if columns[idx] == "movie_id":
                        args.append(movie_id)
                        continue
                    print(f"Enter a value for property {columns[idx]} in {i}:")
                    try:
                        # Let's find out what type of data corresponds to this column that we are dealing with
                        if type(types[idx]) == int:
                            args.append(str(int(input())))
                        elif type(types[idx]) == float:
                            args.append(str(float(input())))
                        elif type(types[idx]) == datetime.date:
                            arg = input()
                            arg = "'{}'".format(datetime.datetime.strptime(arg.strip("'"), '%Y-%m-%d').date())
                        elif type(types[idx]) == str:
                            args.append("\"" + input() + "\"")
                    except ValueError as e:
                        # If the user screwed up entering data, continue
                        print(e)
                        print("One or more values failed. Proceeding to next table.")
                        args = []
                        break
                    except KeyboardInterrupt:
                        # User didn't really want to enter data into this table, skip. 
                        print("Skipping this table.")
                        args = []
                        break
                if len(args) != 0:
                    # If we actually have something to insert into the relationship table, insert.
                    self.cursor.execute(f"INSERT INTO {i} VALUES({','.join(args)});")
            else:
                print(f"Not adding an {i} entry.")
        self.connector.commit()

    def generate_delete_movie(self):
        movie_id = "\"" + input("Enter the movie_id that uniquely identifies the movie.\n") + "\""
      
        FK_Tables = ["ClientInfo", "Features", "Finances", "IMDBRatings", "TMDBRatings", "MoneyInfo", "MoviesCountries", "MoviesCrew", "MoviesGenres", "MoviesKeywords", "MoviesLanguages"]
       
        # We can technically just check the movie_id at the end, in one commit. If it fails, our try/except in main() will catch it and we will continue.
        # We need to delete all tuples in relationships that have reference this movie. 
        for i in FK_Tables:
            self.cursor.execute(f"SELECT * FROM {i} WHERE movie_id={movie_id}")
            movie = self.cursor.fetchone()
            self.cursor.fetchall()
            if movie is not None:
                self.cursor.execute(f"DELETE FROM {i} WHERE movie_id={movie_id}")

        # We finally delete our movie.
        self.cursor.execute(f"DELETE FROM Movies WHERE movie_id={movie_id}")
        # The FK tables delete first, and then finally our movie.
        self.connector.commit()


def main():
    # Some very sketchy authentication. This is merely to suggest and allude to a possible point in the code for authentication.
    print("Input credentials. If you would like to browse as a guest, simply enter guest for username.")
    username = input("Username: (for ECE356 testing purposes, username is 'super' for super privileges.)\n")

    user = None
    if username == "guest":
        # No auth needed, they want to be a guest.
        user = User()
    elif username == "super":
        password = None
        for _ in range(3): #Give user 3 chances to enter the correct password
            password = input("Password: (for ECE356 testing purposes, password is 'password' for super privileges.)\n")
            if password == "password":
                print("Password accepted")
                break
            print("Incorrect password")
            password = None
        if password is None:
            # Password is still None, i.e. incorrect, after three tries. Exit.
            print("Rejected credentials. Exiting.")
            return -1
        # They got it - they are an auth user.
        user = AuthUser()
    else:
        # We have no idea who this is. Not a super user, not a guest.
        print("User unknown. Exiting.")
        return -1

    command = ""

    # Help string that shows us what commands are available for ALL users
    help_string = """
    sm: search movies
    sp: search people
    exit: exit program
    """
    # Help string that shows us what commands are available for AUTHED users
    auth_string = """
    Additional commands for authenticated user:
    arw: add a review and rating
    rrw: rewrite a review and rating
    drw: delete a review and rating
    vrw: view a review and rating
    cm: create a movie entry
    dm: delete a movie entry
    """
    query = ""
    while command.lower() != "exit":
        print(help_string)
        if user.authed:
            print(auth_string)

        command = input("What would you like to do?\n").lower().strip()
        try:
            if command == "sm":
                user.generate_movie_search()
            elif command == "sp":
                user.generate_people_search()
            elif user.authed: # Only if the user is authed, let them do these things
                if command == "arw":
                    user.generate_add_review()
                elif command == "rrw":
                    user.generate_rewrite_review()
                elif command == "drw":
                    user.generate_delete_review()
                elif command == "vrw":
                    user.generate_view_review()
                elif command == "cm":
                    user.generate_create_movie()
                elif command == "dm":
                    user.generate_delete_movie()
                elif command != "exit":
                    # Command wasn't any of the non-auth commands, and it wasn't any of the auth commands, and it wasn't exit. 
                    # Don't know what it was, tell the user, then restart the loop
                    print("Unrecognized argument. Please try again.")
            elif command != "exit":
                # Command wasn't any of the non-auth commands
                # Don't know what it was, tell the user, then restart the loop
                print("Unrecognized argument. Please try again.")
        except KeyboardInterrupt:
            # This is here to catch users that are ctrl+C'ing out of the commands in the helpstring/authstring, to return to the menu.
            print("Command cancelled. Returning to menu.\n\n")
        except mysql.connector.errors.ProgrammingError as e:
            # If we got a SQL error, don't quite crash the program - tell them what screwed up, so that in a practical scenario, they can tell the developer.
            print(f"SQL Error: {e}\nReturning to menu,\n\n")
        # There are some exceptions/errors we don't handle, but they are so erroneous we do want them to crash the program.

if __name__ == "__main__":
    main()


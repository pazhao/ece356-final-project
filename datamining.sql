-- gets top 1000 movies by gross along with budges
select budget,worldwide_gross into outfile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/money.csv' 
fields terminated by ',' 
from moneyinfo order by worldwide_gross desc limit 1000; 

-- gets top 1000 along with avg gross of the director
with big as (select title,worldwide_gross,screen_name from movies 
                inner join moneyinfo using(movie_id) 
                inner join features using(movie_id) 
                inner join person using(person_id) 
                order by worldwide_gross desc limit 1000), 
    big2 as (select screen_name,avg(worldwide_gross) as avggross from big 
                group by screen_name order by avggross desc) 
select distinct screen_name,avggross,worldwide_gross into outfile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/moneyactor.csv' 
fields terminated by ',' 
from moneyinfo 
    inner join moviescrew using(movie_id) 
    inner join person using(person_id) 
    inner join big2 using(screen_name) 
    order by worldwide_gross desc limit 1000;

-- top 1000 along with avg gross of starring actor
with big as (select title,worldwide_gross,screen_name from movies 
                inner join moneyinfo using(movie_id) 
                inner join moviescrew using(movie_id) 
                inner join person using(person_id) 
                where person_role='director' order by worldwide_gross desc limit 1000), 
    big2 as (select screen_name,avg(worldwide_gross) as avggross from big 
                group by screen_name order by avggross desc) 
select screen_name,avggross,worldwide_gross into outfile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/moneydirector.csv' 
fields terminated by ',' from moneyinfo 
    inner join moviescrew using(movie_id) 
    inner join person using(person_id) 
    inner join big2 using(screen_name) 
    order by worldwide_gross desc limit 1000;
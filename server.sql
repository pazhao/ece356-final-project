-- IMPORTANT: Before running, ensure you have created a new database and are currently using that database.

START TRANSACTION;

-- Entities
-- These should be self-explanatory from the ER diagram
CREATE TABLE Movies (
    movie_id varchar(10),
    adult BOOLEAN,
    title varchar(128), -- based on quick analysis, should not exceed 64 characters
    release_date date,
    duration smallint,
    description text,
    mpaa varchar(16),
    PRIMARY KEY (movie_id)
);

CREATE TABLE Person (
    person_id varchar(10),
    screen_name varchar(64), -- name is a reserved keyword so we used another name for the attribute
    birth_name varchar(64),
    height int,
    birth_info_dob date,
    birth_info_birthplace varchar(256),
    birth_info_birth_details text,
    PRIMARY KEY (person_id)
);

CREATE TABLE DeathInfo (
    person_id varchar(10),
    date_of_death date,
    death_place varchar(256),
    death_reason varchar(256),
    death_details text,
    FOREIGN KEY (person_id) REFERENCES Person(person_id),
    PRIMARY KEY (person_id)
);

CREATE TABLE MoneyInfo (
    movie_id varchar(10),
    budget int, 
    domestric_gross int,
    international_gross int,
    worldwide_gross int,
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    PRIMARY KEY (movie_id)
);

CREATE TABLE ClientInfo (
    movie_id varchar(10),
    custom_rating float,
    custom_review text,
    favorited boolean,
    CHECK (custom_rating >= 0 and custom_rating <= 10),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    PRIMARY KEY (movie_id)
);

CREATE TABLE IMDBRatings (
    movie_id varchar(10),
    average_rating float,
    total_votes int,
    top_raters_rating float,
    top_raters_number int,
    number_of_ratings int,
    reviews_from_users int,
    reviews_from_critics int,
    CHECK (average_rating >= 0 and average_rating <= 10),
    CHECK (top_raters_rating >= 0 and top_raters_rating <= 10),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    PRIMARY KEY (movie_id)
);

CREATE TABLE TMDBRatings (
    movie_id varchar(10),
    average_rating float,
    number_of_ratings int,
    CHECK (average_rating >= 0 and average_rating <= 10),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    PRIMARY KEY (movie_id)
);

-- The following tables are used as fk values only
-- The purpose they serve is to avoid the same data with different spellings
CREATE TABLE ProductionCompanies (
    company_name varchar(256),
    PRIMARY KEY (company_name)
);

CREATE TABLE Countries (
    country varchar(64), -- longest country name is like 50 something chars long
    PRIMARY KEY (country)
);

CREATE TABLE Languages (
    lang varchar(64), -- gonna guess that language names are not that long
    PRIMARY KEY (lang)
);

CREATE TABLE Genres (
    genre varchar(32),
    PRIMARY KEY (genre)
);

CREATE TABLE Keywords (
    keyword varchar(64),
    PRIMARY KEY (keyword)
);

-- Job entity in the ER diagram
CREATE TABLE CrewRoles (
    crew_role varchar(32),
    PRIMARY KEY (crew_role)
);
INSERT INTO CrewRoles (crew_role) VALUES ('writer'),('director'),('cinematographer'),('composer'),('producer'),('production_designer'),('editor'),('archive_footage'),('archive_sound');

-- Relations
-- Finances relation between production companies and movies
CREATE TABLE Finances (
    movie_id varchar(10),
    company_name varchar(64),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    FOREIGN KEY (company_name) REFERENCES ProductionCompanies(company_name),
    PRIMARY KEY (movie_id, company_name)
);

-- Cast relation between Persons and Movies
CREATE TABLE Features (
    movie_id varchar(10),
    person_id varchar(10),
    person_character varchar(128), -- role is a reserved keyword so we had to use another name
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    FOREIGN KEY (person_id) REFERENCES Person(person_id),
    PRIMARY KEY (movie_id, person_id, person_character) -- since person_role is actually multi-valued we need each to be a separate entry in the table
);

-- Crew relation between Job, Persons, Movies
CREATE TABLE MoviesCrew (
    movie_id varchar(10),
    person_id varchar(10),
    person_role varchar(32), -- role is a reserved keyword so we had to use another name
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    FOREIGN KEY (person_id) REFERENCES Person(person_id),
    FOREIGN KEY (person_role) REFERENCES CrewRoles(crew_role),
    PRIMARY KEY (movie_id, person_id, person_role) -- since person_role is actually multi-valued we need each to be a separate entry in the table
);

-- Describes relation between Movies and Keywords
CREATE TABLE MoviesKeywords (
    movie_id varchar(10),
    keyword varchar(64),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id),
    FOREIGN KEY (keyword) REFERENCES Keywords(keyword),
    PRIMARY KEY (movie_id, keyword)
);

-- Classification relation between movies and genre
CREATE TABLE MoviesGenres (
    movie_id varchar(10),
    genre varchar(32), -- cannot imagine a genre that is more than 32 characters long
    PRIMARY KEY (movie_id, genre),
    FOREIGN KEY (genre) REFERENCES Genres(genre),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id)
);

-- Filmed in relation between country and movies
CREATE TABLE MoviesCountries (
    movie_id varchar(10),
    country varchar(64),
    PRIMARY KEY (movie_id, country),
    FOREIGN KEY (country) REFERENCES Countries(country),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id)
);

-- Filmed in relation between languages and movies
CREATE TABLE MoviesLanguages (
    movie_id varchar(10),
    lang varchar(64),
    PRIMARY KEY (movie_id, lang),
    FOREIGN KEY (lang) REFERENCES Languages(lang),
    FOREIGN KEY (movie_id) REFERENCES Movies(movie_id)
);

-- Loading data from csv
-- Movies
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/IMDb_movies.csv' 
    INTO TABLE Movies
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (movie_id, 
    title, 
    @dummy, @dummy, 
    release_date, 
    @dummy, 
    duration, 
    @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, -- @dummy for skipping certain CSV columns that we don't need
    description, 
    @dummy, @dummy);

-- creating intermediate tables for temporary data
CREATE TABLE MoviesMetadata (
    movie_id varchar(10),
    adult boolean
);

CREATE TABLE MojoBudgetUpdate (
    movie_id varchar(10),
    mpaa varchar(16)
);

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/movies_metadata.csv' 
    INTO TABLE MoviesMetadata
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (adult, 
    @dummy, @dummy, @dummy, @dummy, @dummy, 
    movie_id);

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/Mojo_budget_update.csv' 
    INTO TABLE MojoBudgetUpdate
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (movie_id,
    @dummy, @dummy, @dummy, 
    mpaa);

UPDATE Movies AS m1, MoviesMetadata AS m2 SET m1.adult=m2.adult WHERE m1.movie_id = m2.movie_id;
UPDATE Movies AS m1, MojoBudgetUpdate AS m2 SET m1.mpaa=m2.mpaa WHERE m1.movie_id = m2.movie_id;

DROP TABLE MoviesMetadata;
DROP TABLE MojoBudgetUpdate;

-- Person
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/formatted_name_data.csv' 
    INTO TABLE Person
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (person_id, 
    screen_name, 
    birth_name, 
    height,
    @dummy, 
    birth_info_birth_details, 
    birth_info_dob, 
    birth_info_birthplace);

-- DeathInfo
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/formatted_name_data.csv'  
    INTO TABLE DeathInfo
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (person_id, 
    @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,
    death_details,
    date_of_death, 
    death_place, 
    death_reason);

-- MoneyInfo
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/processed_money.csv'
    INTO TABLE MoneyInfo
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

-- ClientInfo
-- there are no CSVs relating to clientinfo as the client has to enter their reviews themselves

-- IMDBRating
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/processed_ratings.csv' 
    INTO TABLE IMDBRatings
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (movie_id, 
    average_rating,
    total_votes,
    reviews_from_users,
    reviews_from_critics,
    top_raters_rating,
    top_raters_number);

-- TMDBRating 
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/movie_alt_ratings.csv' 
    INTO TABLE TMDBRatings
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

-- ProductionCompanies
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/production_companies.csv' 
    INTO TABLE ProductionCompanies
    FIELDS TERMINATED BY ',' 
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

-- Countries
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/countries.csv' 
    INTO TABLE Countries
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

-- Languages
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/languages.csv' 
    INTO TABLE Languages
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

-- Genres
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/genres.csv' 
    INTO TABLE Genres
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;
    
-- Keywords
LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/keywords_list.csv' 
    INTO TABLE Keywords
    FIELDS TERMINATED BY ',' 
    ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/processed_keywords.csv' 
    INTO TABLE MoviesKeywords
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/movie_genres.csv' 
    INTO TABLE MoviesGenres
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/movie_production_companies.csv' 
    INTO TABLE Finances
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/movie_countries.csv' 
    INTO TABLE MoviesCountries
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/movie_languages.csv' 
    INTO TABLE MoviesLanguages
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/processed_actors.csv' 
    INTO TABLE Features
    FIELDS TERMINATED BY ','
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES
    (movie_id, 
    person_id,
    @dummy,
    person_character);

LOAD DATA INFILE 'C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/processed_data/processed_roles.csv' 
    INTO TABLE MoviesCrew
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\r\n'
    IGNORE 1 LINES;

COMMIT;
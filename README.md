# ECE356 Final Project

Fall 2021 ECE356, Group 26. Chosen dataset: Movies.
## Contents of the repository
[This repository](https://git.uwaterloo.ca/pazhao/ece356-final-project) contains:

- Final written report
- Code for client-side application
- SQL code for relational schema (table creation, loading data)
- Code for data-mining
- Required CSV files
- An enlarged copy of our ER diagram 

Please note that our test cases were implemented manually, and as such we do not have them in code. However, they are detailed in the written report (page 7 and 12) and can be easily reproduced. 

## Client-side set-up
If `Python (version 3.6+)` and `mysql.connector` are not installed, please install them via pip or another package manager. 
Then, please change the database credentials (under `class DB`) to be appropriate for the machine that you are running it on. 

To use the program: 

    python3 client.py

## Server-side set-up
Start up a SQL session, please create a fresh database (for example, `MyDatabase`) and use that database with the command `\u MyDatabase`. 

Additionally, ensure that all the required CSV files (everything under the `/data/processed_data` folder as well as the following three CSV) are loaded into the correct folder with `--secure-file-priv` enabled: 

- movies_metadata.csv
- Mojo_budget_update.csv
- IMDb_movies.csv

The last three files are from the Kaggle datasets, and the `/processed_data` folder contains our own generated CSV to handle erroneous entries in the original files.

Finally, please search for all occurrences of:

    C:/Users/Aviv/Documents/Code/356prj/ece356-final-project/data/

and replace it with the appropriate path.

## Troubleshooting
The default line endings for our CSV files when loading in data are set to be `\r\n`, this should work as a similar command was posted by TA Liuyang Ren in [Piazza post @560](https://piazza.com/class/krwa21xarxg253?cid=560). However, if running the script ends in empty tables, the line endings are likely wrong. 
If this happens, please search for all occurences of `\r\n` and replace them with `\n`.